package finaltask;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class Driver {
    private final WebDriver webDriver;
    private final WebDriverWait wait;

    public Driver() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver");
        webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();
        wait = new WebDriverWait(webDriver, 30, 5000);
    }

    public WebDriver getDriver() {
        return webDriver;
    }

    public WebElement getElementByCssSelector(String cssSelector) {
        return wait.until(d -> webDriver.findElement(By.cssSelector(cssSelector)));
    }

    public void clickOnElementByCssSelector(String cssSelector) {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(cssSelector))).click();
    }

    public void clickOnElementByCssSelectorAndWaitTextChanged(String cssSelector, String cssSelectorTextNode, String text) {
        getElementByCssSelector(cssSelector).click();
        WebElement textNode = getElementByCssSelector(cssSelectorTextNode);
        wait.until(ExpectedConditions.not(ExpectedConditions.textToBePresentInElement(textNode, text)));
    }

    public String getTextFromElementByCssSelector(String cssSelector) {
        WebElement element = getElementByCssSelector(cssSelector);
        String text = element.getText().trim();
        List<WebElement> children = element.findElements(By.xpath("./*"));
        for (WebElement child : children) {
            text = text.replaceFirst(child.getText(), "").trim();
        }
        return text;
    }

    public String getRawTextFromElementByCssSelector(String cssSelector) {
        WebElement element = getElementByCssSelector(cssSelector);
        return element.getText();
    }

    public int getIntFromElementByCssSelector(String cssSelector) {
        return Integer.parseInt(getTextFromElementByCssSelector(cssSelector).replaceAll("\\s+", ""));
    }

    public WebElement getElementById(String id) {
        return wait.until(d -> webDriver.findElement(By.id(id)));
    }

    public void clickOnElementById(String id) {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id(id))).click();
    }

    public void setTextToElementByCssSelector(String cssSelector, CharSequence... keysToSend) {
        getElementByCssSelector(cssSelector).clear();
        getElementByCssSelector(cssSelector).sendKeys(keysToSend);
        getElementByCssSelector(cssSelector).sendKeys(Keys.ENTER);
    }

    public void setTextToElementById(String id, CharSequence... keysToSend) {
        getElementById(id).clear();
        getElementById(id).sendKeys(keysToSend);
        getElementById(id).sendKeys(Keys.ENTER);
    }

    public List<WebElement> getElementsListByCssSelector(String cssSelector) {
        return wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector(cssSelector)));
    }

    public String getAttributeByElement(WebElement element, String attrName) {
        return element.getAttribute(attrName);
    }

    public void waitForUrlContains(String expectedString) {
        ExpectedCondition<Boolean> urlIsCorrect = arg0 -> webDriver.getCurrentUrl().contains(expectedString);
        wait.until(urlIsCorrect);
    }

    public void waitForProfileLoaded(String profileCssSelector) {
        WebElement profileLink = webDriver.findElement(By.cssSelector(profileCssSelector));
        wait.until(ExpectedConditions.not(ExpectedConditions.textToBePresentInElement(profileLink, " войдите в личный кабинет ")));
    }

    public String getCurrentUrl() {
        return webDriver.getCurrentUrl();
    }

    public void openUrl(String url) {
        webDriver.get(url);
    }

    public void quit() {
        webDriver.quit();
    }
}
