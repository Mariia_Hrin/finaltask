package finaltask.tests.negativetests;

import finaltask.listener.TestListener;
import finaltask.pages.HomePage;
import finaltask.pages.ProductPage;
import finaltask.pages.ResultSearchPage;
import finaltask.pages.ShoppingCartPage;
import finaltask.tests.Asserts;
import finaltask.tests.BaseTest;
import io.qameta.allure.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners({TestListener.class})
@Epic(value = "Rozetka website functional testing")
@Feature(value = "Shopping Cart Actions")
public class ShopCartPageNegativeTests extends BaseTest {
    private static final String SEARCH_TEXT = "пылесос";
    private static final int ID = 0;

    private HomePage homePage;
    private ResultSearchPage resultSearchPage;
    private ProductPage productPage;
    private ShoppingCartPage shoppingCartPage;

    @BeforeClass
    public void setUp() {
        homePage = new HomePage(driver);
        resultSearchPage = new ResultSearchPage(driver);
        productPage = new ProductPage(driver);
        shoppingCartPage = new ShoppingCartPage(driver);
    }

    @Test(groups = "negative")
    @Owner(value = "Mariia Grin")
    @Description(value = "The test is checking entering invalid count of product in the basket")
    @Story(value = "Enter invalid count of product in the basket")
    @Severity(value = SeverityLevel.NORMAL)
    public void selectWrongProductCountTest() {
        homePage.goToHomePage();
        homePage.search(SEARCH_TEXT);

        resultSearchPage.goToProductPage(ID);

        productPage.addProductToTheCart();

        shoppingCartPage.enterInvalidProductCount();

        boolean actualResponse = shoppingCartPage.errorMessageIsDisplayed();
        String errorMessage = shoppingCartPage.getErrorMessage();

        Asserts.errorMessageIsDisplayed(actualResponse, errorMessage);
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }
}
