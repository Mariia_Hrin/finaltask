package finaltask.tests.negativetests;

import finaltask.listener.TestListener;
import finaltask.pages.AuthorizationPage;
import finaltask.pages.HomePage;
import finaltask.pages.ResultSearchPage;
import finaltask.tests.Asserts;
import finaltask.tests.BaseTest;
import io.qameta.allure.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners({TestListener.class})
@Epic(value = "Rozetka website functional testing")
@Feature(value = "Header menu")
public class HomePageNegativeTests extends BaseTest {
    private static final String SEARCH_TEXT = "sdfsgd";
    private static final String WRONG_CITY = "hgjhgj";
    private final String errorMessage = "Город не найден";

    private HomePage homePage;
    private ResultSearchPage resultSearchPage;
    private AuthorizationPage authorizationPage;

    @BeforeClass
    public void setUp() {
        homePage = new HomePage(driver);
        resultSearchPage = new ResultSearchPage(driver);
        authorizationPage = new AuthorizationPage(driver);
        homePage.goToHomePage();
        authorizationPage.goToLoginPage();
        authorizationPage.enterEmail();
        authorizationPage.enterPassword();
        authorizationPage.clickAuthorizationButton();
    }

    @Test(groups = "negative")
    @Owner(value = "Mariia Grin")
    @Description(value = "The test is checking products searching with wrong request")
    @Story(value = "Searching products with invalid data")
    @Severity(value = SeverityLevel.NORMAL)
    public void productSearchWithWrongRequestTest() {
        resultSearchPage = homePage.searchFromHome(SEARCH_TEXT);

        boolean responseContentIsDisplayed = resultSearchPage.contentSearchNothingIsDisplayed();
        String errorMessage = resultSearchPage.getMessageNothingIsDisplayed();

        Asserts.contentSearchNothingIsDisplayed(responseContentIsDisplayed, errorMessage);
    }

    @Test(groups = "negative")
    @Owner(value = "Mariia Grin")
    @Description(value = "The test is checking selecting city with invalid data")
    @Story(value = "Select city with invalid data")
    @Severity(value = SeverityLevel.NORMAL)
    public void enterWrongCityNameTest() {
        homePage.enterCityName(WRONG_CITY);
        String actualMessage = homePage.getErrorMessage();

        Asserts.checkErrorMessage(actualMessage, errorMessage);
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }
}
