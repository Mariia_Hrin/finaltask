package finaltask.tests.negativetests;

import finaltask.listener.TestListener;
import finaltask.pages.*;
import finaltask.tests.Asserts;
import finaltask.tests.BaseTest;
import io.qameta.allure.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners({TestListener.class})
@Epic(value = "Rozetka website functional testing")
@Feature(value = "Wishlist")
public class WishlistNegativeTest extends BaseTest {
    private static final String SEARCH_TEXT = "пылесос";
    private static final int ID = 0;

    HomePage homePage;
    AuthorizationPage authorizationPage;
    ResultSearchPage resultSearchPage;
    ProductPage productPage;
    WishlistPage wishlistPage;


    @BeforeClass
    public void setUp() {
        homePage = new HomePage(driver);
        authorizationPage = new AuthorizationPage(driver);
        resultSearchPage = new ResultSearchPage(driver);
        productPage = new ProductPage(driver);
        wishlistPage = new WishlistPage(driver);
    }

    @Test(groups = "negative")
    @Owner(value = "Mariia Grin")
    @Description(value = "The test is checking adding selected product to the wishlist by unauthorized user")
    @Story(value = "Add product to the wishlist by unauthorized user")
    @Severity(value = SeverityLevel.MINOR)
    public void addProductToTheWishlistUnauthorizedUserTest() {
        homePage.goToHomePage();
        homePage.search(SEARCH_TEXT);

        resultSearchPage.goToProductPage(ID);

        productPage.addProductToTheWishlist();

        boolean actualResult = authorizationPage.isPageLoaded();

        Asserts.isAuthorizationPageLoaded(actualResult);
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }
}
