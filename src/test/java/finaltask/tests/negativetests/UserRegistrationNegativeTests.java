package finaltask.tests.negativetests;

import finaltask.listener.TestListener;
import finaltask.pages.AuthorizationPage;
import finaltask.pages.HomePage;
import finaltask.pages.UserRegistrationPage;
import finaltask.tests.Asserts;
import finaltask.tests.BaseTest;
import io.qameta.allure.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners({TestListener.class})
@Epic(value = "Rozetka website functional testing")
@Feature(value = "User registration")
public class UserRegistrationNegativeTests extends BaseTest {
    HomePage homePage;
    AuthorizationPage authorizationPage;
    UserRegistrationPage userRegistrationPage;
    private String errorMessageForInvalidData = "на кириллице";
    private String errorMessageForPassword = "ng-invalid";

    @BeforeMethod
    public void setUp() {
        homePage = new HomePage(driver);
        authorizationPage = new AuthorizationPage(driver);
        userRegistrationPage = new UserRegistrationPage(driver);
        homePage.goToHomePage();
        authorizationPage.goToUserRegistrationForm();
    }

    @Test(groups = "negative")
    @Owner(value = "Mariia Grin")
    @Description(value = "The test is checking registration existing user")
    @Story(value = "Registration existing user")
    @Severity(value = SeverityLevel.CRITICAL)
    public void registrationExistingUserTest() {
        userRegistrationPage.enterSurname();
        userRegistrationPage.enterName();
        userRegistrationPage.enterExistingUserName();
        userRegistrationPage.enterUserPassword();
        userRegistrationPage.clickOnRegistrationButton();

        boolean actualResult = userRegistrationPage.checkUserRegistration();

        Asserts.registrationExistingUser(actualResult);
    }

    @Test(groups = "negative")
    @Owner(value = "Mariia Grin")
    @Description(value = "The test is checking user registration with invalid surname and name")
    @Story(value = "User registration with invalid surname and name")
    @Severity(value = SeverityLevel.CRITICAL)
    public void UserRegistrationWithInvalidDataTest() {
        userRegistrationPage.enterInvalidSurnameAndName();
        userRegistrationPage.enterExistingUserName();
        userRegistrationPage.enterUserPassword();
        userRegistrationPage.clickOnRegistrationButton();

        String errorMessageForInvalidUserData = userRegistrationPage.errorMessageForInvalidUserData();

        Asserts.registrationWithInvalidData(errorMessageForInvalidUserData, errorMessageForInvalidData);
    }

    @Test(groups = "negative")
    @Owner(value = "Mariia Grin")
    @Description(value = "The test is checking user registration with invalid password")
    @Story(value = "User authorization with invalid password")
    @Severity(value = SeverityLevel.CRITICAL)
    public void UserRegistrationWithInvalidPasswordTest() {
        userRegistrationPage.enterSurname();
        userRegistrationPage.enterName();
        userRegistrationPage.enterExistingUserName();
        userRegistrationPage.enterInvalidPassword();
        userRegistrationPage.clickOnRegistrationButton();

        String actualErrorMessage = userRegistrationPage.errorMessageForInvalidUserPassword();

        Asserts.registrationWithInvalidPassword(actualErrorMessage, errorMessageForPassword);
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }
}
