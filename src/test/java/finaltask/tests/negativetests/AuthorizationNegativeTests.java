package finaltask.tests.negativetests;

import finaltask.listener.TestListener;
import finaltask.pages.AuthorizationPage;
import finaltask.pages.HomePage;
import finaltask.tests.Asserts;
import finaltask.tests.BaseTest;
import io.qameta.allure.*;
import org.testng.annotations.*;

@Listeners({TestListener.class})
@Epic(value = "Rozetka website functional testing")
@Feature(value = "User authorization")
public class AuthorizationNegativeTests extends BaseTest {
    private final String errorMessageForInvalidEmail = "неверный адрес эл.почты";
    private final String errorMessageForInvalidPass = "неверный пароль";
    private final String errorMessageForUnregUser = "ng-invalid";

    HomePage homePage;
    AuthorizationPage authorizationPage;

    @BeforeClass
    public void setUpClass() {
        homePage = new HomePage(driver);
        authorizationPage = new AuthorizationPage(driver);
    }
    @BeforeMethod
    public void setUp() {
        homePage.goToHomePage();
    }

    @Test(groups = "negative")
    @Owner(value = "Mariia Grin")
    @Description(value = "The test is checking user authorization with invalid email")
    @Story(value = "User authorization with invalid email")
    @Severity(value = SeverityLevel.CRITICAL)
    public void userAuthorizationWithInvalidLoginTest() {
        authorizationPage.goToLoginPage();
        authorizationPage.enterInvalidEmail();
        authorizationPage.enterPassword();
        authorizationPage.clickAuthorizationButton();

        String actualMessage = authorizationPage.errorMessageForInvalidLogin();

        Asserts.checkErrorMessage(actualMessage, errorMessageForInvalidEmail);
    }

    @Test(groups = "negative")
    @Owner(value = "Mariia Grin")
    @Description(value = "The test is checking user authorization with invalid password")
    @Story(value = "User authorization with invalid password")
    @Severity(value = SeverityLevel.CRITICAL)
    public void userAuthorizationWithInvalidPasswordTest() {
        authorizationPage.goToLoginPage();
        authorizationPage.enterEmail();
        authorizationPage.enterInvalidPassword();
        authorizationPage.clickAuthorizationButton();

        String actualMessage = authorizationPage.errorMessageForInvalidPassword();

        Asserts.checkErrorMessage(actualMessage, errorMessageForInvalidPass);
    }

    @Test(groups = "negative")
    @Owner(value = "Mariia Grin")
    @Description(value = "The test is checking unregistered user authorization")
    @Story(value = "Unregistered user authorization")
    @Severity(value = SeverityLevel.CRITICAL)
    public void authorizationUnregisteredUserTest() {
        authorizationPage.goToLoginPage();
        authorizationPage.enterUnregisteredUserEmail();
        authorizationPage.enterPassword();
        authorizationPage.clickAuthorizationButton();

        String actualMessage = authorizationPage.unregisteredErrorMessage();

        Asserts.checkErrorMessage(actualMessage, errorMessageForUnregUser);
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }
}
