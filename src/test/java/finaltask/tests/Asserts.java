package finaltask.tests;

import io.qameta.allure.Step;

import static org.testng.Assert.*;

public class Asserts {

    @Step("Check the error message contains {expectedMessage}")
    public static void checkErrorMessage(String actualMessage, String expectedMessage) {
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Step("Check user {userName} are displayed in the profile")
    public static void checkUserIsLogin(boolean answer, String userName) {
        assertTrue(answer);
    }

    @Step("Check message - {errorMessage} on result search page is displayed")
    public static void contentSearchNothingIsDisplayed(boolean answer, String errorMessage) {
        assertTrue(answer);
    }

    @Step("Check the searching products are displayed")
    public static void searchingProductsDisplayed(String actualResult, String expectedResult) {
        assertTrue(actualResult.contains(expectedResult));
    }

    @Step("Get selected city name {selectedCity} and compare with the selected city name from the cities list {selectedCityInHeader}")
    public static void compareSelectedCities(String selectedCity, String selectedCityInHeader) {
        assertEquals(selectedCity, selectedCityInHeader);
    }

    @Step("Check error message {errorMessage} is displayed")
    public static void errorMessageIsDisplayed(boolean answer, String errorMessage) {
        assertTrue(answer);
    }

    @Step("Compare the product name had to be removed and the name of the deleted product")
    public static void checkDeleteProductFromCart(String productNameHadToBeDeleted, String deletedProductName) {
        assertFalse(productNameHadToBeDeleted.contains(deletedProductName));
    }

    @Step("Check the total sum of product price was changed after increasing the product count in the shopping cart")
    public static void checkTotalSumIsChanged(int totalSumAfterIncreasing, int totalSumBeforeIncreasing, int productPrice) {
        assertEquals(totalSumAfterIncreasing, totalSumBeforeIncreasing + productPrice);
    }

    @Step("Compare the product name had to added to the shopping cart and the name of the added product")
    public static void checkAddProductToCart(String productNameHadToBeAdded, String addedProductName) {
        assertEquals(productNameHadToBeAdded, addedProductName);
    }

    @Step("Check is the user personal information was changed after editing")
    public static void checkUserPersonalInfo(String userPersonalInfo, String USER_SURNAME) {
        assertTrue(userPersonalInfo.contains(USER_SURNAME));
    }

    @Step("Check is the selected product from selected promotion category")
    public static void checkTheSelectProductFromSelectedPromotion(String promotionName, String promotionText){
        assertTrue(promotionName.contains(promotionText));
    }

    @Step("Check is the selected product deleted from wishlist")
    public static void checkIsTheProductDeleted (String productTitle, String deletedProductName){
        assertFalse(productTitle.contains(deletedProductName));
    }

    @Step("Check is the selected product added to the wishlist")
    public static void checkIsTheProductAdded (String productTitleInWishlist, String addedProductTitle){
        assertEquals(productTitleInWishlist, addedProductTitle);
    }
    @Step("Check that unauthorized user can't added product to wishlist")
    public static void isAuthorizationPageLoaded (boolean actualResult){
        assertTrue(actualResult);
    }

    @Step("Check the registration existing user")
    public static void registrationExistingUser (boolean isExistingUser){
        assertTrue(isExistingUser);
    }

    @Step("Check the error message for invalid data is displayed")
    public static void registrationWithInvalidData (String errorMessageForInvalidUserData, String errorMessageForInvalidData){
        assertTrue(errorMessageForInvalidUserData.contains(errorMessageForInvalidData));
    }

    @Step("Check the error message for invalid password is displayed")
    public static void registrationWithInvalidPassword (String actualErrorMessage, String errorMessageForPassword){
        assertTrue(actualErrorMessage.contains(errorMessageForPassword));
    }

    @Step("Compare the viewed products in the basket with the viewed products on the viewed products page")
    public static void compareCountsViewedProducts (int countViewedProductsInBasket, int countViewedProductsInViewedPage){
        assertEquals(countViewedProductsInBasket, countViewedProductsInViewedPage);
    }

}
