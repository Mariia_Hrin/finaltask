package finaltask.tests.positivetests;

import finaltask.listener.TestListener;
import finaltask.pages.HomePage;
import finaltask.pages.ProductPage;
import finaltask.pages.ResultSearchPage;
import finaltask.pages.ShoppingCartPage;
import finaltask.tests.Asserts;
import finaltask.tests.BaseTest;
import io.qameta.allure.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners({TestListener.class})
@Epic(value = "Rozetka website functional testing")
@Feature(value = "Actions with selected product")
public class ProductPageTests extends BaseTest {
    private static final String SEARCH_TEXT = "пылесос";
    private static final int ID = 0;

    private HomePage homePage;
    private ResultSearchPage resultSearchPage;
    private ProductPage productPage;
    private ShoppingCartPage shoppingCartPage;

    @BeforeClass
    public void setUp() {
        homePage = new HomePage(driver);
        resultSearchPage = new ResultSearchPage(driver);
        productPage = new ProductPage(driver);
        shoppingCartPage = new ShoppingCartPage(driver);
    }

    @Test(groups = "positive")
    @Owner(value = "Mariia Grin")
    @Description(value = "The test is checking adding product to the basket")
    @Story(value = "Add product to the basket")
    @Severity(value = SeverityLevel.CRITICAL)
    public void addProductToTheBasketTest() {
        homePage.goToHomePage();
        homePage.search(SEARCH_TEXT);

        resultSearchPage.goToProductPage(ID);
        productPage.addProductToTheCart();

        String productNameHadToBeAdded = productPage.getProductTitle();
        String addedProductName = shoppingCartPage.getTitleOfProduct();

        Asserts.checkAddProductToCart(addedProductName, productNameHadToBeAdded);
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }
}
