package finaltask.tests.positivetests;

import finaltask.listener.TestListener;
import finaltask.pages.*;
import finaltask.tests.Asserts;
import finaltask.tests.BaseTest;
import io.qameta.allure.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners({TestListener.class})
@Epic(value = "Rozetka website functional testing")
@Feature(value = "Viewed products")
public class ViewedProductsPageTest extends BaseTest {
    HomePage homePage;
    ShoppingCartPage shoppingCartPage;
    AuthorizationPage authorizationPage;
    ViewedProductsPage viewedProductsPage;

    @BeforeClass
    public void setUp() {
        homePage = new HomePage(driver);
        shoppingCartPage = new ShoppingCartPage(driver);
        authorizationPage = new AuthorizationPage(driver);
        viewedProductsPage = new ViewedProductsPage(driver);
        homePage.goToHomePage();
    }

    @Test(groups = "positive")
    @Owner(value = "Mariia Grin")
    @Description(value = "The test is checking viewed products")
    @Story(value = "Checking viewed products")
    @Severity(value = SeverityLevel.MINOR)
    public void checkingViewedProductsTest() {
        authorizationPage.goToLoginPage();
        authorizationPage.enterEmail();
        authorizationPage.enterPassword();
        authorizationPage.clickAuthorizationButton();

        homePage.goToShoppingCartPage();
        int countViewedProductsInBasket = shoppingCartPage.getViewedProductsInTheBasket().size();

        shoppingCartPage.goToViewedProductsPage();
        int countViewedProductsInViewedPage = viewedProductsPage.getViewedProductsOnViewedPage().size();

        Asserts.compareCountsViewedProducts(countViewedProductsInBasket, countViewedProductsInViewedPage);
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }
}
