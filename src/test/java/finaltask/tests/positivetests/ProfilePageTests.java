package finaltask.tests.positivetests;

import finaltask.listener.TestListener;
import finaltask.pages.AuthorizationPage;
import finaltask.pages.HomePage;
import finaltask.pages.ProfilePage;
import finaltask.tests.Asserts;
import finaltask.tests.BaseTest;
import io.qameta.allure.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners({TestListener.class})
@Epic(value = "Rozetka website functional testing")
@Feature(value = "User profile")
public class ProfilePageTests extends BaseTest {
    private static final String USER_SURNAME = "Петрушевич";

    private HomePage homePage;
    private AuthorizationPage authorizationPage;
    private ProfilePage profilePage;

    @BeforeClass
    public void setUp() {
        homePage = new HomePage(driver);
        authorizationPage = new AuthorizationPage(driver);
        profilePage = new ProfilePage(driver);
        homePage.goToHomePage();
    }

    @Test(groups = "positive")
    @Owner(value = "Mariia Grin")
    @Description(value = "The test is checking user's profile editing")
    @Story(value = "Edit user personal information in the profile")
    @Severity(value = SeverityLevel.MINOR)
    public void editPersonalInfoTest() {
        authorizationPage.goToLoginPage();
        authorizationPage.enterEmail();
        authorizationPage.enterPassword();
        authorizationPage.clickAuthorizationButton();
        authorizationPage.goToProfilePage();

        profilePage.editPersonalInfo(USER_SURNAME);
        profilePage.clickOnSaveButton();

        String userNameAndSurname = profilePage.getUserNameAndSurname();

        Asserts.checkUserPersonalInfo(userNameAndSurname, USER_SURNAME);
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }
}
