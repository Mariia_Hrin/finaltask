package finaltask.tests.positivetests;

import finaltask.listener.TestListener;
import finaltask.pages.HomePage;
import finaltask.pages.ProductPage;
import finaltask.pages.PromotionsPage;
import finaltask.pages.SelectedPromotionPage;
import finaltask.tests.Asserts;
import finaltask.tests.BaseTest;
import io.qameta.allure.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners({TestListener.class})
@Epic(value = "Rozetka website functional testing")
@Feature(value = "Promotions products")
public class PromotionPageTests extends BaseTest {
    private static final int ID = 2;

    private HomePage homePage;
    private PromotionsPage promotionsPage;
    private SelectedPromotionPage selectedPromotionPage;
    private ProductPage productPage;

    @BeforeClass
    public void setUp() {
        homePage = new HomePage(driver);
        promotionsPage = new PromotionsPage(driver);
        selectedPromotionPage = new SelectedPromotionPage(driver);
        productPage = new ProductPage(driver);
    }

    @Test(groups = "positive")
    @Owner(value = "Mariia Grin")
    @Description(value = "The test is checking selecting product from promotions")
    @Story(value = "Select product from promotions")
    @Severity(value = SeverityLevel.CRITICAL)
    public void selectProductFromPromotionsTest() {
        homePage.goToHomePage();
        homePage.goToPromotionsPage();

        promotionsPage.selectPromotion(ID);

        String promotionName = selectedPromotionPage.getSelectedPromotionTitle();
        selectedPromotionPage.selectPromotionProduct(ID);

        String promotionTitle = productPage.getPromotionText();

        Asserts.checkTheSelectProductFromSelectedPromotion(promotionName, promotionTitle);
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }
}
