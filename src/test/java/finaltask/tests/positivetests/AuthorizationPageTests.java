package finaltask.tests.positivetests;

import finaltask.listener.TestListener;
import finaltask.pages.AuthorizationPage;
import finaltask.pages.HomePage;
import finaltask.tests.Asserts;
import finaltask.tests.BaseTest;
import io.qameta.allure.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners({TestListener.class})
@Epic(value = "Rozetka website functional testing")
@Feature(value = "User authorization")
public class AuthorizationPageTests extends BaseTest {
    private HomePage homePage;
    private AuthorizationPage authorizationPage;

    @BeforeClass
    public void setUp() {
        authorizationPage = new AuthorizationPage(driver);
        homePage = new HomePage(driver);
        homePage.goToHomePage();
    }

    @Test(groups = "positive")
    @Owner(value = "Mariia Grin")
    @Description(value = "The test is checking user authorization")
    @Story(value = "User authorization with valid data")
    @Severity(value = SeverityLevel.CRITICAL)
    public void userAuthorizationTest() {
        authorizationPage.goToLoginPage();
        authorizationPage.enterEmail();
        authorizationPage.enterPassword();
        authorizationPage.clickAuthorizationButton();

        boolean answer = authorizationPage.checkUserIsLogIn();
        String userName = authorizationPage.getUserNameAndSurname();

        Asserts.checkUserIsLogin(answer, userName);
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }
}
