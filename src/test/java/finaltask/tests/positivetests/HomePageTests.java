package finaltask.tests.positivetests;

import finaltask.listener.TestListener;
import finaltask.pages.AuthorizationPage;
import finaltask.pages.HomePage;
import finaltask.pages.ResultSearchPage;
import finaltask.tests.Asserts;
import finaltask.tests.BaseTest;
import io.qameta.allure.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.io.UnsupportedEncodingException;

@Listeners({TestListener.class})
@Epic(value = "Rozetka website functional testing")
@Feature(value = "Header menu")
public class HomePageTests extends BaseTest {
    private static final String SEARCH_TEXT = "пылесос";
    private static final String CITY = "Днепр";
    private static final int ID = 0;

    private HomePage homePage;
    private ResultSearchPage resultSearchPage;
    private AuthorizationPage authorizationPage;

    @BeforeClass
    public void setUp() {
        homePage = new HomePage(driver);
        resultSearchPage = new ResultSearchPage(driver);
        authorizationPage = new AuthorizationPage(driver);
        homePage.goToHomePage();
    }

    @Test(groups = "positive")
    @Owner(value = "Mariia Grin")
    @Description(value = "The test is checking products searching")
    @Story(value = "Searching products with valid data")
    @Severity(value = SeverityLevel.NORMAL)
    public void productSearchTest() throws UnsupportedEncodingException {
        authorizationPage.goToLoginPage();
        authorizationPage.enterEmail();
        authorizationPage.enterPassword();
        authorizationPage.clickAuthorizationButton();

        resultSearchPage = homePage.search(SEARCH_TEXT);

        String linkText = resultSearchPage.getLinkText();

        Asserts.searchingProductsDisplayed(linkText, SEARCH_TEXT);
    }

    @Test(groups = "positive")
    @Owner(value = "Mariia Grin")
    @Description(value = "The test is checking selected city")
    @Story(value = "Select city with valid data")
    @Severity(value = SeverityLevel.NORMAL)
    public void selectCityTest() {
        authorizationPage.goToLoginPage();
        authorizationPage.enterEmail();
        authorizationPage.enterPassword();
        authorizationPage.clickAuthorizationButton();

        homePage.enterCityName(CITY);

        String selectedCity = homePage.selectCity(ID);
        String cityInHeader = homePage.getSelectedCityInHeader();

        Asserts.compareSelectedCities(selectedCity, cityInHeader);
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }
}
