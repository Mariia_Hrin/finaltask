package finaltask.tests.positivetests;

import finaltask.listener.TestListener;
import finaltask.pages.HomePage;
import finaltask.pages.ProductPage;
import finaltask.pages.ResultSearchPage;
import finaltask.pages.ShoppingCartPage;
import finaltask.tests.Asserts;
import finaltask.tests.BaseTest;
import io.qameta.allure.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners({TestListener.class})
@Epic(value = "Rozetka website functional testing")
@Feature(value = "Shopping Cart Actions")
public class ShoppingCartPageTest extends BaseTest {
    private static final String SEARCH_TEXT = "пылесос";
    private static final int ID = 0;

    private HomePage homePage;
    private ResultSearchPage resultSearchPage;
    private ProductPage productPage;
    private ShoppingCartPage shoppingCartPage;

    @BeforeClass
    public void setUp() {
        homePage = new HomePage(driver);
        resultSearchPage = new ResultSearchPage(driver);
        productPage = new ProductPage(driver);
        shoppingCartPage = new ShoppingCartPage(driver);
    }

    @Test(groups = "positive")
    @Owner(value = "Mariia Grin")
    @Description(value = "The test is checking deleting selected product from the basket")
    @Story(value = "Delete selected product from the basket")
    @Severity(value = SeverityLevel.NORMAL)
    public void deleteProductFromTheBasketTest() {
        homePage.goToHomePage();
        homePage.search(SEARCH_TEXT);

        resultSearchPage.goToProductPage(ID);
        productPage.addProductToTheCart();

        String deletedProductName = shoppingCartPage.deleteProductFromTheBasket(ID);
        String productNameHadToBeDeleted = shoppingCartPage.getTitleOfProduct();

        Asserts.checkDeleteProductFromCart(productNameHadToBeDeleted, deletedProductName);
    }

    @Test(groups = "positive")
    @Owner(value = "Mariia Grin")
    @Description(value = "The test is checking increasing count of product in the basket")
    @Story(value = "Increase count of product in the basket")
    @Severity(value = SeverityLevel.NORMAL)
    public void increaseProductCountTest() {
        homePage.goToHomePage();
        homePage.search(SEARCH_TEXT);

        resultSearchPage.goToProductPage(ID);
        productPage.addProductToTheCart();

        int totalSumBeforeIncreasing = shoppingCartPage.getTotalSum();
        int productPrice = shoppingCartPage.getProductPrice();
        String productPriceStr = shoppingCartPage.getProductPriceString();

        shoppingCartPage.increaseProductCount(productPriceStr);
        int totalSumAfterIncreasing = shoppingCartPage.getTotalSum();

        Asserts.checkTotalSumIsChanged(totalSumAfterIncreasing, totalSumBeforeIncreasing, productPrice);
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }
}
