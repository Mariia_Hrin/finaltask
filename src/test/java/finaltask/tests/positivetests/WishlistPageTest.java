package finaltask.tests.positivetests;

import finaltask.listener.TestListener;
import finaltask.pages.*;
import finaltask.tests.Asserts;
import finaltask.tests.BaseTest;
import io.qameta.allure.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners({TestListener.class})
@Epic(value = "Rozetka website functional testing")
@Feature(value = "Wishlist")
public class WishlistPageTest extends BaseTest {
    private static final String SEARCH_TEXT = "самокат";
    private static final int DELETE_ID = 0;
    private static final int ADD_ID = 1;

    HomePage homePage;
    AuthorizationPage authorizationPage;
    ResultSearchPage resultSearchPage;
    ProductPage productPage;
    WishlistPage wishlistPage;


    @BeforeClass
    public void setUp() {
        homePage = new HomePage(driver);
        authorizationPage = new AuthorizationPage(driver);
        resultSearchPage = new ResultSearchPage(driver);
        productPage = new ProductPage(driver);
        wishlistPage = new WishlistPage(driver);

        homePage.goToHomePage();
        authorizationPage.goToLoginPage();
        authorizationPage.enterEmail();
        authorizationPage.enterPassword();
        authorizationPage.clickAuthorizationButton();
        authorizationPage.waitForProfile();
    }

    @Test(groups = "positive", priority = 1)
    @Owner(value = "Mariia Grin")
    @Description(value = "The test is checking deleting selected product from the wishlist")
    @Story(value = "Delete product from the wishlist")
    @Severity(value = SeverityLevel.NORMAL)
    public void deleteProductFromTheWishlistTest() {
        homePage.search(SEARCH_TEXT);
        resultSearchPage.goToProductPage(ADD_ID);

        productPage.addProductToTheWishlist();
        productPage.goToWishlistPage();

        String productTitle = wishlistPage.getTitleOfProduct();
        String deletedProductName = wishlistPage.deleteProductFromTheWishlist(DELETE_ID);

        Asserts.checkIsTheProductDeleted(productTitle, deletedProductName);
    }

    @Test(groups = "positive", priority = 2)
    @Owner(value = "Mariia Grin")
    @Description(value = "The test is checking adding selected product to the wishlist")
    @Story(value = "Add product to the wishlist")
    @Severity(value = SeverityLevel.NORMAL)
    public void addProductToTheWishlistTest() {
        homePage.search(SEARCH_TEXT);
        resultSearchPage.goToProductPage(ADD_ID);

        String addedProductTitle = productPage.getProductTitle();
        productPage.addProductToTheWishlist();
        productPage.goToWishlistPage();

        String productTitleInWishlist = wishlistPage.getTitleOfProduct();

        Asserts.checkIsTheProductAdded(productTitleInWishlist, addedProductTitle);
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }
}
