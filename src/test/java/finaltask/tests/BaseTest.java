package finaltask.tests;

import finaltask.Driver;
import org.openqa.selenium.WebDriver;

public class BaseTest {

    protected Driver driver = new Driver();
    public WebDriver getDriver(){
        return driver.getDriver();
    }
}
