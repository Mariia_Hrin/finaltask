package finaltask.pages;

import finaltask.Driver;
import io.qameta.allure.Step;

public class PromotionsPage extends BasePage {
    private final String promotionsLinks = "ul.promo-grid>li:nth-child(%s) a";

    public PromotionsPage(Driver driver) {
        super(driver);
    }

    @Step("Select promotion")
    public SelectedPromotionPage selectPromotion(int id) {
        driver.clickOnElementByCssSelector(String.format(promotionsLinks, id + 1));
        return new SelectedPromotionPage(driver);
    }
}
