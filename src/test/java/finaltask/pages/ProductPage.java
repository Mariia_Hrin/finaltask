package finaltask.pages;

import finaltask.Driver;
import io.qameta.allure.Step;

public class ProductPage extends BasePage {
    private final String buyButton = "button.buy-button:nth-child(1)";
    private final String productTitle = "h1.product__title";
    private final String addToWishlistButton = "li.product-actions__item button.wish-button";
    private final String promotionTextField = "p.product-promotion__text";
    private final String wishlistButton = "a[href*='wishlists'].header-actions__button";
    private final String userProfileLink = "a.header-topline__user-link";

    public ProductPage(Driver driver) {
        super(driver);
    }

    @Step("Get product title")
    public String getProductTitle() {
        return driver.getTextFromElementByCssSelector(productTitle);
    }

    @Step("Click buy button")
    public ShoppingCartPage addProductToTheCart() {
        driver.clickOnElementByCssSelector(buyButton);
        return new ShoppingCartPage(driver);
    }

    @Step("Click button add to wishlist")
    public void addProductToTheWishlist() {
        driver.waitForProfileLoaded(userProfileLink);
        driver.clickOnElementByCssSelector(addToWishlistButton);
    }

    @Step("Navigate to wishlist page")
    public WishlistPage goToWishlistPage() {
        driver.clickOnElementByCssSelector(wishlistButton);
        return new WishlistPage(driver);
    }

    @Step("Get promotion title")
    public String getPromotionText() {
        return driver.getTextFromElementByCssSelector(promotionTextField);
    }
}
