package finaltask.pages;

import finaltask.Driver;
import io.qameta.allure.Step;

public class AuthorizationPage extends BasePage {
    private final static String EMAIL = "usertest20202021@gmail.com";
    private final static String INVALID_EMAIL = "hvghhggmail.com";
    private final static String UNREGISTERED_EMAIL = "hvghhg@gmail.com";
    private final static String PASSWORD = "ut99876QW67";
    private final static String INVALID_PASSWORD = " ";

    private final String userLoginLink = "a.header-topline__user-link";
    private final String loginInput = "auth_email";
    private final String passwordInput = "auth_pass";
    private final String authorizButton = "button.auth-modal__submit";
    private final String userRegistrationLink = "a.auth-modal__register-link";
    private final String userProfileLink = "a.header-topline__user-link[rzabchecker]";
    private final String errorMessage = "p.error-message";
    private final String errorMessageForm = "div.form__hint_type_warning>strong";
    private final String authorizationForm = "form.auth-modal__form";
    private final String attrName = "class";
    private final String profileLink = "li.main-auth__links-item>a[href*='personal-information']";

    public AuthorizationPage(Driver driver) {
        super(driver);
    }

    @Step("Navigate to user registration page")
    public void goToUserRegistrationForm() {
        driver.clickOnElementByCssSelector(userLoginLink);
        driver.clickOnElementByCssSelector(userRegistrationLink);
    }

    @Step("Navigate to user login page")
    public void goToLoginPage() {
        driver.clickOnElementByCssSelector(userLoginLink);
    }

    @Step("Enter user email")
    public void enterEmail() {
        driver.setTextToElementById(loginInput, EMAIL);
    }

    @Step("Enter user password")
    public void enterPassword() {
        driver.setTextToElementById(passwordInput, PASSWORD);
        driver.clickOnElementByCssSelector(authorizButton);
    }

    @Step("Click authorization button")
    public void clickAuthorizationButton() {
        driver.clickOnElementByCssSelector(authorizButton);
    }

    public void waitForProfile() {
        driver.waitForProfileLoaded(userLoginLink);
    }

    @Step("Enter invalid user email")
    public void enterInvalidEmail() {
        driver.setTextToElementById(loginInput, INVALID_EMAIL);
    }

    @Step("Enter invalid user password")
    public void enterInvalidPassword() {
        driver.setTextToElementById(passwordInput, INVALID_PASSWORD);
    }

    @Step("Enter unregistered user email")
    public void enterUnregisteredUserEmail() {
        driver.setTextToElementById(loginInput, UNREGISTERED_EMAIL);
    }

    public String errorMessageForInvalidLogin() {
        return driver.getTextFromElementByCssSelector(errorMessage);
    }

    public String unregisteredErrorMessage() {
        return driver.getAttributeByElement(driver.getElementByCssSelector(authorizationForm), attrName);
    }

    public String errorMessageForInvalidPassword() {
        return driver.getTextFromElementByCssSelector(errorMessageForm);
    }

    public boolean checkUserIsLogIn() {
        return driver.getElementByCssSelector(userProfileLink).isEnabled();
    }

    public String getUserNameAndSurname() {
        return driver.getTextFromElementByCssSelector(userProfileLink);
    }

    public boolean isPageLoaded() {
        return driver.getElementById(loginInput).isDisplayed();
    }

    @Step("Go to user profile page")
    public ProfilePage goToProfilePage() {
        driver.clickOnElementByCssSelector(profileLink);
        return new ProfilePage(driver);
    }
}
