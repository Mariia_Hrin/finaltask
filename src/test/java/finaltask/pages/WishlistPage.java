package finaltask.pages;

import finaltask.Driver;
import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;

import java.util.List;

public class WishlistPage extends BasePage {
    private final String productTitleLink = "a.goods-tile__heading";
    private final String title = "title";
    private final String productsInWishlist = "ul.wish-grid>li";
    private final String actionsInWishlist = "div.tile-checkbox:nth-child(%s)";
    private final String deleteButton = "button.js-goods-delete";

    public WishlistPage(Driver driver) {
        super(driver);
    }

    public String getTitleOfProduct() {
        WebElement productTitle = driver.getElementByCssSelector(productTitleLink);
        return driver.getAttributeByElement(productTitle, title);
    }

    @Step("Delete product from the wishlist")
    public String deleteProductFromTheWishlist(int id) {
        List<WebElement> productsInTheWishlist = driver.getElementsListByCssSelector(productsInWishlist);
        String productTitle = productsInTheWishlist.get(id).getText();

        driver.clickOnElementByCssSelector(String.format(actionsInWishlist, id + 1));
        driver.clickOnElementByCssSelector(deleteButton);

        return productTitle;
    }
}
