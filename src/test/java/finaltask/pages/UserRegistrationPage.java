package finaltask.pages;

import finaltask.Driver;
import io.qameta.allure.Step;

public class UserRegistrationPage extends BasePage {
    private static final String SURNAME = "Петрушевич";
    private static final String NAME = "Артем";
    private static final String USER_NAME = "kjvczxsayeq@gmail.com";
    private static final String PASSWORD = "ebzaqw54321AFF";
    private static final String INVALID_SURNAME = "cghfhgf";
    private static final String INVALID_NAME = "kjk";
    private static final String INVALID_PASSWORD = "kljlkj";

    private final String surnameInput = "[formcontrolname=surname]";
    private final String nameInput = "[formcontrolname=name]";
    private final String usernameInput = "[formcontrolname=username]";
    private final String passwordInput = "[formcontrolname=password]";
    private final String registrationButton = "button.auth-modal__submit";
    private final String errorMessageField = "p.validation-message";
    private final String attrName = "class";

    public UserRegistrationPage(Driver driver) {
        super(driver);
    }

    @Step("Enter surname")
    public void enterSurname() {
        driver.setTextToElementByCssSelector(surnameInput,SURNAME);
    }

    @Step("Enter name")
    public void enterName() {
        driver.setTextToElementByCssSelector(nameInput, NAME);
    }

    @Step("Enter registered username")
    public void enterExistingUserName() {
        driver.setTextToElementByCssSelector(usernameInput, USER_NAME);
    }

    @Step("Enter user password")
    public void enterUserPassword() {
        driver.setTextToElementByCssSelector(passwordInput, PASSWORD);
    }

    @Step("Click on registration button")
    public void clickOnRegistrationButton() {
        driver.clickOnElementByCssSelector(registrationButton);
    }

    @Step("Enter invalid surname and name")
    public void enterInvalidSurnameAndName() {
        driver.setTextToElementByCssSelector(surnameInput,INVALID_SURNAME);
        driver.setTextToElementByCssSelector(nameInput, INVALID_NAME);
    }

    @Step("Enter invalid password")
    public void enterInvalidPassword() {
        driver.setTextToElementByCssSelector(passwordInput, INVALID_PASSWORD);
    }

    @Step("Check error message is displayed")
    public boolean checkUserRegistration(){
        return driver.getElementByCssSelector(errorMessageField).isDisplayed();
    }

    @Step("Check error message for invalid data is displayed")
    public String errorMessageForInvalidUserData(){
        return driver.getTextFromElementByCssSelector(errorMessageField);
    }

    @Step("Check error message is displayed")
    public String errorMessageForInvalidUserPassword(){
        return driver.getAttributeByElement(driver.getElementByCssSelector(passwordInput), attrName);
    }
}
