package finaltask.pages;

import finaltask.Driver;
import io.qameta.allure.Step;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

public class ResultSearchPage extends BasePage {
    private final String productsCatalog = "ul.catalog-grid>li:nth-child(%s) a.goods-tile__picture";
    private final String contentSearchNothing = "div.search-nothing__wrap";
    private final String messageNothingIsDisplayed = "div.search-nothing__wrap p:last-child";

    public ResultSearchPage(Driver driver) {
        super(driver);
    }

    public String getLinkText() throws UnsupportedEncodingException {
        return URLDecoder.decode(driver.getCurrentUrl(), "UTF-8");
    }

    @Step("Navigate to product page")
    public ProductPage goToProductPage(int id) {
        driver.clickOnElementByCssSelector(String.format(productsCatalog, id + 1));
        return new ProductPage(driver);
    }

    public boolean contentSearchNothingIsDisplayed() {
        return driver.getElementByCssSelector(contentSearchNothing).isDisplayed();
    }

    public String getMessageNothingIsDisplayed() {
        return driver.getTextFromElementByCssSelector(messageNothingIsDisplayed);
    }
}
