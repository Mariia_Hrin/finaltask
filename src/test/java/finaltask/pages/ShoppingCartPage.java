package finaltask.pages;

import finaltask.Driver;
import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;

import java.util.List;

public class ShoppingCartPage extends BasePage {
    private final String productTitle = "a.cart-product__title";
    private final String actionsInBasket = ".cart-list .cart-list__item:nth-child(%s) button[aria-controls='shoppingCartActions']";
    private final String productsInBasket = "ul.cart-list>li";
    private final String deleteButton = "#shoppingCartActions>li button:nth-child(1)";
    private final String addButton = "button[aria-label*='Добавить']";
    private final String productPrice= "p.cart-product__price";
    private final String totalSum = "div.cart-receipt__sum-price";
    private final String inputForProductCount ="input.cart-counter__input";
    private final String errorMessage = "p.cart-product__tech_color_red";
    private final String showMoreViewedProductsLink = "a.recently-viewed__show-more";
    private final String viewedProducts = "ul.simple-slider__list>li";

    public ShoppingCartPage(Driver driver) {
        super(driver);
    }

    @Step("Delete product from the shopping cart")
    public String deleteProductFromTheBasket(int id) {
        List<WebElement> productsInTheBasket = driver.getElementsListByCssSelector(productsInBasket);
        String productTitle = productsInTheBasket.get(id).getText();

        driver.clickOnElementByCssSelector(String.format(actionsInBasket, id+1));
        driver.clickOnElementByCssSelector(deleteButton);

        return productTitle;
    }

    public String getTitleOfProduct() {
        return driver.getTextFromElementByCssSelector(productTitle);
    }

    @Step("Increase count of product in the shopping cart")
    public void increaseProductCount(String price){
        driver.clickOnElementByCssSelectorAndWaitTextChanged(addButton, productPrice, price);
    }

    @Step("Navigate to viewed products page")
    public ViewedProductsPage goToViewedProductsPage(){
        driver.clickOnElementByCssSelector(showMoreViewedProductsLink);
        return new ViewedProductsPage(driver);
    }

    public List<WebElement> getViewedProductsInTheBasket(){
        return driver.getElementsListByCssSelector(viewedProducts);
    }

    public int getProductPrice(){
        return driver.getIntFromElementByCssSelector(productPrice);
    }

    public String getProductPriceString(){
        return driver.getRawTextFromElementByCssSelector(productPrice);
    }

    public int getTotalSum(){
        return driver.getIntFromElementByCssSelector(totalSum);
    }

    @Step("Enter invalid count of product in the shopping cart")
    public void enterInvalidProductCount(){
        driver.getElementByCssSelector(inputForProductCount).clear();
        driver.setTextToElementByCssSelector(inputForProductCount, "0");
    }

    public boolean errorMessageIsDisplayed(){
        return driver.getElementByCssSelector(errorMessage).isDisplayed();
    }

    public String getErrorMessage(){
        return driver.getTextFromElementByCssSelector(errorMessage);
    }
}
