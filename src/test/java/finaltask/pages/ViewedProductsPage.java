package finaltask.pages;

import finaltask.Driver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class ViewedProductsPage extends BasePage {
    private final String viewedProducts = "ul.viewed-grid>li";


    public ViewedProductsPage(Driver driver) {
        super(driver);
    }

    public List<WebElement> getViewedProductsOnViewedPage(){
        return driver.getElementsListByCssSelector(viewedProducts);
    }

}
