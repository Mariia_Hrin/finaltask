package finaltask.pages;

import finaltask.Driver;
import io.qameta.allure.Step;

public class SelectedPromotionPage extends BasePage {
    private final String promotionProductLinks = "ul.catalog-grid>li:nth-child(%s) a.goods-tile__picture";
    private final String promotionTitle = "h1.promo-hero__heading>span";

    public SelectedPromotionPage(Driver driver) {
        super(driver);
    }

    @Step("Select promotion product")
    public ProductPage selectPromotionProduct(int id) {
        driver.clickOnElementByCssSelector(String.format(promotionProductLinks, id+1));
        return new ProductPage(driver);
    }

    public String getSelectedPromotionTitle() {
        return driver.getTextFromElementByCssSelector(promotionTitle);
    }
}
