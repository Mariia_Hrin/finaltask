package finaltask.pages;

import finaltask.Driver;
import io.qameta.allure.Step;

public class ProfilePage extends BasePage {
    private final String editPersonalInfoButton = "button.personal-section__edit";
    private final String inputSurname = "lastName";
    private final String submitButton = "button[type=submit]";
    private final String userNameAndSurname = "p.cabinet-user__name";

    public ProfilePage(Driver driver) {
        super(driver);
    }

    @Step("Edit user personal information")
    public void editPersonalInfo(String lastName){
        driver.clickOnElementByCssSelector(editPersonalInfoButton);
        driver.clickOnElementById(inputSurname);
        driver.getElementById(inputSurname).clear();
        driver.setTextToElementById(inputSurname, lastName);
    }

    @Step("Save edited personal information")
    public void clickOnSaveButton(){
        driver.clickOnElementByCssSelector(submitButton);
    }


    @Step("Check user name and surname are updated")
    public String getUserNameAndSurname(){
        return driver.getTextFromElementByCssSelector(userNameAndSurname);
    }
}
