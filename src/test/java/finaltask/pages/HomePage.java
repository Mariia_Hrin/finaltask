package finaltask.pages;

import finaltask.Driver;
import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;

import java.util.List;

public class HomePage extends BasePage {
    private static final String BASE_URL = "https://rozetka.com.ua";
    private final String promotionsLink = "a.main-slider__pagination-link";
    private final String searchField = "input.search-form__input";
    private final String headerCitiesLink = "a.header-cities__link";
    private final String cityField = "input.autocomplete__input";
    private final String citySelector = "ul.autocomplete__list>li:nth-child(%s)";
    private final String citiesLayer = "ul.autocomplete__list>li";
    private final String applyButton = "div.header-location__footer button";
    private final String errorMessage = "p.error-message";
    private final String basketLink = "a.header-actions__button_type_basket";

    public HomePage(Driver driver) {
        super(driver);
    }

    @Step("Navigate to home page")
    public void goToHomePage() {
        driver.openUrl(BASE_URL);
    }

    @Step("Navigate to shopping cart page")
    public ShoppingCartPage goToShoppingCartPage() {
        driver.clickOnElementByCssSelector(basketLink);
        return new ShoppingCartPage(driver);
    }

    @Step("Search product")
    public ResultSearchPage search(String searchText) {
        driver.setTextToElementByCssSelector(searchField, searchText);
        driver.waitForUrlContains("search_text");
        return new ResultSearchPage(driver);
    }

    @Step("Search product")
    public ResultSearchPage searchFromHome(String searchText) {
        driver.setTextToElementByCssSelector(searchField, searchText);
        driver.waitForUrlContains("search");
        return new ResultSearchPage(driver);
    }

    @Step("Enter city name")
    public void enterCityName(String city) {
        driver.clickOnElementByCssSelector(headerCitiesLink);
        driver.clickOnElementByCssSelector(cityField);

        driver.setTextToElementByCssSelector(cityField, city);
    }

    @Step("Select city from the cities list")
    public String selectCity(int id) {
        List<WebElement> cities = driver.getElementsListByCssSelector(citiesLayer);

        String selectedCity = cities.get(id).getText();
        driver.clickOnElementByCssSelector(String.format(citySelector, id + 1));
        driver.clickOnElementByCssSelector(applyButton);

        return selectedCity;
    }

    public String getSelectedCityInHeader() {
        return driver.getTextFromElementByCssSelector(headerCitiesLink);
    }

    public String getErrorMessage() {
        return driver.getTextFromElementByCssSelector(errorMessage);
    }

    @Step("Navigate to promotions page")
    public PromotionsPage goToPromotionsPage() {
        driver.clickOnElementByCssSelector(promotionsLink);
        driver.waitForUrlContains("promotions");
        return new PromotionsPage(driver);
    }
}
